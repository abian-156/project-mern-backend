const products = [
    {
     name: 'Apple iPhone 12 Pro Max 256GB',
      image: '/images/iphone12promax.jpg',
      description:
        'Splash, Water, and Dust Resistant	All-glass and surgical-grade stainless steel design, water and dust resistant rated IP68,Triple 12MP cameras with Portrait mode, Depth Control, Portrait Lighting, Smart HDR 3, and 4K Dolby Vision HDR video up to 60 fps',
      brand: 'Apple',
      category: 'Electronics',
      price: 1999.99,
      countInStock: 0,
      rating: 4,
      numReviews: 4,
      
    },
    {
      name: 'Apple iPhone 13 Mini 128GB, Pink',
      image: '/images/iphone13red.webp',
      description:
        'Display 6.1-inch Super Retina XDR display with True Tone,Splash, Water, and Dust Resistant	All-glass and surgical-grade stainless steel design, water and dust resistant rated IP68 - maximum depth of 6 meters up to 30 minutes',
      brand: 'Apple',
      category: 'Electronics',
      price: 899.99,
      countInStock: 10,
      rating: 4.0,
      numReviews: 4,
    },
    {
      name: 'Apple iPhone 13 Mini 128GB, Pink',
      image: '/images/iphone13mini.jpg',
      description:
        'Display 6.1-inch Super Retina XDR display with True Tone,Splash, Water, and Dust Resistant	All-glass and surgical-grade stainless steel design, water and dust resistant rated IP68 - maximum depth of 6 meters up to 30 minutes',
      brand: 'Apple',
      category: 'Electronics',
      price: 499.99,
      countInStock: 7,
      rating: 3.5,
      numReviews: 2,
      
    },
    {
      name: 'iPhone 12 mini',
      image: '/images/iphone12mini.webp',
      description:
        'Dual 12MP cameras with Portrait mode, Depth Control, Portrait Lighting, Smart HDR, and 4K Dolby Vision HDR video up to 30 fps, 12MP TrueDepth front camera with Portrait mode, Depth Control, Portrait Lighting, and Smart HDR 3',
      brand: 'Apple',
      category: 'Electronics',
      price: 699.99,
      countInStock: 10,
      rating: 5,
      numReviews: 3,
    },
    {
     name: 'Apple Watch Series 7',
      image: '/images/applewatch.jpg',
      description:
        'style: GPS, size: 41mm color: Green Aluminum Case with Clover Sport Band',
      brand: 'Apple',
      category: 'Electronics',
      price: 329.99,
      countInStock: 5,
      rating: 3,
      numReviews: 3,
    },
    {
    name: 'Apple AirPods (3rd Generation)',
      image: '/images/airpodspro.jpg',
      description:
        'Bluetooth technology lets you connect it with compatible devices wirelessly High-quality AAC audio offers immersive listening experience Built-in microphone allows you to take calls while working',
      brand: 'Apple',
      category: 'Electronics',
      price: 89.99,
      countInStock: 3,
      rating: 4.5,
      numReviews: 4,
    },
  ]
  
  export default products